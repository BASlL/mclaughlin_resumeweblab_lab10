var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view account_view as
 select s.*, a.street, a.zip_code from account s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'CALL account_getinfo2(?)';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


/*
exports.insert = function(params, callback) {

    // FIRST INSERT THE ACCOUNT
    var query = 'INSERT INTO account (first_name, last_name, email) VALUES (?,?,?)';
    var queryData = [params.first_name, params.last_name, params.email];

    connection.query(query, queryData, function(err, result) {
        var query = 'INSERT INTO account_school (school_id) VALUES (?)';
        var queryData = [params.school_id];
        connection.query(query, queryData, function(err, result) {
            var query = 'INSERT INTO account_company (company_id) VALUES (?)';
            var queryData = [params.company_id];
            connection.query(query, queryData, function(err, result) {
                var query = 'INSERT INTO account_skill (skill_id) VALUES (?)';
                var queryData = [params.skill_id];
                connection.query(query, queryData, function(err, result) {
                    callback(err, result);
                });
            });
        });
    });
};
*/

exports.insert = function (params, callback) {
    // FIRST INSERT THE ACCOUNT
    var query = 'INSERT INTO account (first_name, last_name, email) VALUES (?,?,?)';

    var queryData = [params.first_name, params.last_name, params.email];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE ACCOUNT_ID RETURNED AS insertId AND THE SELECTED SCHOOL_IDs INTO ACCOUNT_SCHOOL
        var account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';
        var query2 = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
        var query3 = 'INSERT INTO account_company (account_id, company_id) VALUES ?';


        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var accountSchoolData = [];
        if (params.school_id.constructor === Array) {
            for (var i = 0; i < params.school_id.length; i++) {
                accountSchoolData.push([account_id, params.school_id[i]]);
            }
        }
        else {
            accountSchoolData.push([account_id, params.school_id]);
        }

        var accountSkillData = [];
        if (params.skill_id.constructor === Array) {
            for (var i = 0; i < params.skill_id.length; i++) {
                accountSkillData.push([account_id, params.skill_id[i]]);
            }
        }
        else {
            accountSkillData.push([account_id, params.skill_id]);
        }

        var accountCompanyData = [];
        if (params.company_id.constructor === Array) {
            for (var i = 0; i < params.company_id.length; i++) {
                accountCompanyData.push([account_id, params.company_id[i]]);
            }
        }
        else {
            accountCompanyData.push([account_id, params.company_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [accountSchoolData], function(err, result){
            connection.query(query2, [accountSkillData], function(err, result){
                connection.query(query3, [accountCompanyData], function(err, result){
                    callback(err, result);
                });
            });
        });
    });
};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var accountSchoolInsert = function(account_id, schoolIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            accountSchoolData.push([account_id, schoolIdArray[i]]);
        }
    }
    else {
        accountSchoolData.push([account_id, schoolIdArray]);
    }
    connection.query(query, [accountSchoolData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountSchoolInsert = accountSchoolInsert;

//declare the function so it can be used locally
var accountSchoolDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_school WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountSchoolDeleteAll = accountSchoolDeleteAll;

//declare the function so it can be used locally
var accountSkillInsert = function(account_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            accountSkillData.push([account_id, skillIdArray[i]]);
        }
    }
    else {
        accountSkillData.push([account_id, skillIdArray]);
    }
    connection.query(query, [accountSkillData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountSkillInsert = accountSkillInsert;

//declare the function so it can be used locally
var accountSkillDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_skill WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountSkillDeleteAll = accountSkillDeleteAll;

//declare the function so it can be used locally
var accountCompanyInsert = function(account_id, companyIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            accountCompanyData.push([account_id, companyIdArray[i]]);
        }
    }
    else {
        accountCompanyData.push([account_id, companyIdArray]);
    }
    connection.query(query, [accountCompanyData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountCompanyInsert = accountCompanyInsert;

//declare the function so it can be used locally
var accountCompanyDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_company WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountCompanyDeleteAll = accountCompanyDeleteAll;

//declare the function so it can be used locally
var accountCompanyDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_company WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountCompanyDeleteAll = accountCompanyDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE account SET first_name = ?, last_name = ?, email = ? WHERE account_id = ?';
    var queryData = [params.first_name, params.last_name, params.email, params.account_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        accountSchoolDeleteAll(params.account_id, function(err, result){

            if(params.school_id != null) {
                //insert company_address ids
                accountSchoolInsert(params.account_id, params.school_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};
exports.updateSkill = function (params, callback) {
    accountSkillDeleteAll(params.account_id, function(err, result){

        if(params.skill_id != null) {
            //insert company_address ids
            accountSkillInsert(params.account_id, params.skill_id, function(err, result){
                callback(err, result);
            });}
        else {
            callback(err, result);
        }
    });
};

exports.updateCompany = function (params, callback) {
    accountCompanyDeleteAll(params.account_id, function(err, result){

        if(params.company_id != null) {
            //insert company_address ids
            accountCompanyInsert(params.account_id, params.company_id, function(err, result){
                callback(err, result);
            });}
        else {
            callback(err, result);
        }
    });
};


/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS account_getinfo;

     DELIMITER //
     CREATE PROCEDURE account_getinfo (_account_id int)
     BEGIN

     SELECT * FROM account WHERE account_id = _account_id;

     SELECT a.*, s.account_id FROM address a
     LEFT JOIN account_address s on s.address_id = a.address_id AND account_id = _account_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL account_getinfo (4);

 */

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
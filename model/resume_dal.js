var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view resume_view as
 select s.*, a.street, a.zip_code from resume s
 join account a on a.account_id = s.account_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM resume_view;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
    var query ='CALL resume_getinfo(?)';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO resume (resume_name, account_id) VALUES (?,?)';
    var queryData = [params.resume_name, params.account_id];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE ACCOUNT_ID RETURNED AS insertId AND THE SELECTED SCHOOL_IDs INTO ACCOUNT_SCHOOL
        var resume_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
        var query2 = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';
        var query3 = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';


        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var resumeSchoolData = [];
        if (params.school_id.constructor === Array) {
            for (var i = 0; i < params.school_id.length; i++) {
                resumeSchoolData.push([resume_id, params.school_id[i]]);
            }
        }
        else {
            resumeSchoolData.push([resume_id, params.school_id]);
        }

        var resumeSkillData = [];
        if (params.skill_id.constructor === Array) {
            for (var i = 0; i < params.skill_id.length; i++) {
                resumeSkillData.push([resume_id, params.skill_id[i]]);
            }
        }
        else {
            resumeSkillData.push([resume_id, params.skill_id]);
        }

        var resumeCompanyData = [];
        if (params.company_id.constructor === Array) {
            for (var i = 0; i < params.company_id.length; i++) {
                resumeCompanyData.push([resume_id, params.company_id[i]]);
            }
        }
        else {
            resumeCompanyData.push([resume_id, params.company_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [resumeSchoolData], function(err, result){
            connection.query(query2, [resumeSkillData], function(err, result){
                connection.query(query3, [resumeCompanyData], function(err, result){
                    callback(err, resume_id);
                });
            });
        });
    });
};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var resumeSchoolInsert = function(resume_id, schoolIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            resumeSchoolData.push([resume_id, schoolIdArray[i]]);
        }
    }
    else {
        resumeSchoolData.push([resume_id, schoolIdArray]);
    }
    connection.query(query, [resumeSchoolData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSchoolInsert = resumeSchoolInsert;

//declare the function so it can be used locally
var resumeSchoolDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_school WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSchoolDeleteAll = resumeSchoolDeleteAll;

//declare the function so it can be used locally
var resumeSkillInsert = function(resume_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            resumeSkillData.push([resume_id, skillIdArray[i]]);
        }
    }
    else {
        resumeSkillData.push([resume_id, skillIdArray]);
    }
    connection.query(query, [resumeSkillData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSkillInsert = resumeSkillInsert;

//declare the function so it can be used locally
var resumeSkillDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSkillDeleteAll = resumeSkillDeleteAll;

//declare the function so it can be used locally
var resumeCompanyInsert = function(resume_id, companyIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            resumeCompanyData.push([resume_id, companyIdArray[i]]);
        }
    }
    else {
        resumeCompanyData.push([resume_id, companyIdArray]);
    }
    connection.query(query, [resumeCompanyData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeCompanyInsert = resumeCompanyInsert;

//declare the function so it can be used locally
var resumeCompanyDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_company WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeCompanyDeleteAll = resumeCompanyDeleteAll;


exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        resumeSchoolDeleteAll(params.resume_id, function(err, result){

            if(params.school_id != null) {
                //insert company_address ids
                resumeSchoolInsert(params.resume_id, params.school_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.updateSkill = function (params, callback) {
    resumeSkillDeleteAll(params.resume_id, function(err, result){

        if(params.skill_id != null) {
            //insert company_address ids
            resumeSkillInsert(params.resume_id, params.skill_id, function(err, result){
                callback(err, result);
            });}
        else {
            callback(err, result);
        }
    });
};

exports.updateCompany = function (params, callback) {
    resumeCompanyDeleteAll(params.resume_id, function(err, result){

        if(params.company_id != null) {
            //insert company_address ids
            resumeCompanyInsert(params.resume_id, params.company_id, function(err, result){
                callback(err, result);
            });}
        else {
            callback(err, result);
        }
    });
};

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo8(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, resume_id) {
        callback(err, resume_id);
    });
};